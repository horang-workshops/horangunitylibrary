using System.Collections.Generic;
using UnityEngine;

public class GenericObjectPool<T> : MonoBehaviour, IObjectPool<T> where T : MonoBehaviour, IPoolable
{
	[SerializeField]
	private T prefab;

	private readonly Stack<T> reusableInstances = new Stack<T>();
	private int count;

	public void ReturnToPool(object instance)
	{
		if (instance is T poolableInstance)
		{
			ReturnToPool(poolableInstance);
		}
	}

	public T GetPrefabInstance()
	{
		T instance;
		
		if (reusableInstances.Count > 0)
		{
			instance = reusableInstances.Pop();
			
			instance.gameObject.SetActive(true);
		}
		else
		{
			instance = Instantiate(prefab);

			instance.name = typeof(T) + " #" + count;
			
			count++;
		}
		
		instance.origin = this;
		instance.transform.SetParent(transform);
		instance.PrepareToUse();

		return instance;
	}

	public void ReturnToPool(T instance)
	{
		instance.gameObject.SetActive(false);
		
		reusableInstances.Push(instance);
	}
}
