public interface IObjectPool
{
	public void ReturnToPool(object instance);
}

public interface IObjectPool<T> : IObjectPool where T : IPoolable
{
	public T GetPrefabInstance();
	public void ReturnToPool(T instance);
}
