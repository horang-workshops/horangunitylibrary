public interface IPoolable
{
	IObjectPool origin { get; set; }

	public void PrepareToUse();
	public void ReturnToPool();
}
