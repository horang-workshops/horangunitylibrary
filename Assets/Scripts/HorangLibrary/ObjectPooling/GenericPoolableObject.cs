using UnityEngine;

public class GenericPoolableObject : MonoBehaviour, IPoolable
{
	public IObjectPool origin { get; set; }
	
	public virtual void PrepareToUse()
	{
	}

	public virtual void ReturnToPool()
	{
		origin.ReturnToPool(this);
	}
}
