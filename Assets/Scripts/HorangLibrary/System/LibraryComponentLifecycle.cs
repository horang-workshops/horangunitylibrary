﻿using UnityEngine;

namespace HorangLibrary
{
	public abstract class LibraryComponentLifecycle : MonoBehaviour
	{
		private void Awake()
		{
			Initialize();
		}

		private void Start()
		{
			LateInitialize();
		}

		private void OnDestroy()
		{
			ObjectDestroy();
		}

		private void Update()
		{
			ObjectUpdate();
		}

		protected virtual void Initialize() {}

		protected virtual void LateInitialize() {}
		
		protected virtual void ObjectDestroy() {}

		protected virtual void ObjectUpdate() {}
	}
}