using System;
using System.IO;

namespace HorangLibrary
{
	public static class TextFileManager
	{
		public static string[] ReadText(string filePath, char splitSeparator)
		{
			var fileInfo = new FileInfo(filePath);
			string readValue;

			if (fileInfo.Exists)
			{
				var streamReader = new StreamReader(filePath);

				readValue = streamReader.ReadToEnd();
			
				streamReader.Close();
			}
			else
			{
				Logger.Error("파일을 찾을 수 없습니다. 경로를 다시 확인해주세요.");
				
				return Array.Empty<string>();
			}

			return readValue.Split(splitSeparator);
		}

		public static void WriteText(string filePath, string message)
		{
			var directoryPath = Path.GetDirectoryName(filePath);

			if (directoryPath is null)
			{
				return;
			}
			
			var directoryInfo = new DirectoryInfo(directoryPath);

			if (!directoryInfo.Exists)
			{
				directoryInfo.Create();
			}

			var fileStream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
			var writer = new StreamWriter(fileStream, System.Text.Encoding.Unicode);

			writer.WriteLine(message);
			
			writer.Close(); 
			fileStream.Close();
		}
	}
}
