using UnityEngine;

namespace HorangLibrary
{
	public static class VectorExtensions
	{
		public static Vector2 X(this Vector2 position, float toChangeValue)
		{
			position.x = toChangeValue;

			return position;
		}

		public static Vector2 Y(this Vector2 position, float toChangeValue)
		{
			position.y = toChangeValue;

			return position;
		}

		public static Vector2 AddX(this Vector2 position, float toAddValue)
		{
			position.x += toAddValue;

			return position;
		}

		public static Vector2 AddY(this Vector2 position, float toAddValue)
		{
			position.y += toAddValue;

			return position;
		}

		public static Vector3 X(this Vector3 position, float toChangeValue)
		{
			position.x = toChangeValue;

			return position;
		}

		public static Vector3 Y(this Vector3 position, float toChangeValue)
		{
			position.y = toChangeValue;

			return position;
		}

		public static Vector3 Z(this Vector3 position, float toChangeValue)
		{
			position.z = toChangeValue;

			return position;
		}

		public static Vector3 AddX(this Vector3 position, float toAddValue)
		{
			position.x += toAddValue;

			return position;
		}

		public static Vector3 AddY(this Vector3 position, float toAddValue)
		{
			position.y += toAddValue;

			return position;
		}

		public static Vector3 AddZ(this Vector3 position, float toAddValue)
		{
			position.z += toAddValue;

			return position;
		}
	}
}