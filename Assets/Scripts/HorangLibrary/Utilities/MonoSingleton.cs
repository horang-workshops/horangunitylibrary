using UnityEngine;

namespace HorangLibrary
{
	public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static bool isShuttingDown;
		private static object lockObject = new object();
		private static object instance;

		public static T Instance
		{
			get
			{
				if (isShuttingDown)
				{
					return null;
				}

				lock (lockObject)
				{
					if (instance is { })
					{
						return instance as T;
					}

					instance = (T)FindObjectOfType(typeof(T));

					if (instance is { })
					{
						return instance as T;
					}

					var singletonGameObject = new GameObject();
					instance = singletonGameObject.AddComponent(typeof(T)) as T;
					singletonGameObject.name = "[SINGLETON] " + typeof(T);

					DontDestroyOnLoad(singletonGameObject);

					return instance as T;
				}
			}
		}

		private void OnApplicationQuit()
		{
			isShuttingDown = true;
		}

		public virtual void OnDestroy()
		{
			isShuttingDown = true;
		}
	}
}