using System.Diagnostics;
using UnityEngine;

namespace HorangLibrary
{
	public static class Logger
	{
		private static string TextSize(this object obj, int fontSize = 13) => $"<size={fontSize}>{obj}</size>";
		private static string TextColor(this object obj, Color color) => $"<color=#{ColorUtility.ToHtmlStringRGBA(color)}>{obj}</color>";
		private static string Text(this object obj, Color color, int fontSize = 13) => obj.TextColor(color).TextSize(fontSize);

		private static string StackTrace(int fontSize)
		{
			var stackFrame = new StackTrace(true).GetFrame(2);
			
			var stackTraceFullPath = stackFrame.GetMethod().ReflectedType?.Name.TextColor(Color.cyan * 0.8f) + ".".TextColor(Color.white) + // 클래스 이름
												stackFrame.GetMethod().Name.TextColor(Color.magenta * 0.8f) + ":".TextColor(Color.white) + // 메서드 명
												stackFrame.GetFileLineNumber().TextColor(Color.green); // 라인 위치
			
			stackTraceFullPath = ("[" + stackTraceFullPath + "]").Text(Color.white, fontSize);
			
			return stackTraceFullPath;
		}

		public static void Verbose(string message, Color color = default, int textSize = 14)
		{
			color = color == default ? Color.white : color;
			
			UnityEngine.Debug.Log(StackTrace(textSize) + Text(" → ", color, textSize) + Text(message, color, textSize));
		}

		public static void Warning(string message, Color color = default, int textSize = 14)
		{
			color = color == default ? Color.white : color;
			
			UnityEngine.Debug.LogWarning(StackTrace(textSize) + Text(" → ", color, textSize) + Text(message, color, textSize));
		}

		public static void Error(string message, Color color = default, int textSize = 14)
		{
			color = color == default ? Color.white : color;
			
			UnityEngine.Debug.LogError(StackTrace(textSize) + Text(" → ", color, textSize) + Text(message, color, textSize));
		}
	}
}