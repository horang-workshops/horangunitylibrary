namespace HorangLibrary
{
	public interface IVisualEffectEntity
	{
		public void Play();
		public void Pause();
		public void Stop();
	}
}
