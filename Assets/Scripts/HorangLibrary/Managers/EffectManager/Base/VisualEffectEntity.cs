﻿using UnityEngine;

namespace HorangLibrary
{
	public class VisualEffectEntity : LibraryComponentLifecycle, IVisualEffectEntity, IPoolable
	{
		public bool IsPlay { get; private set; }
		public bool IsPause { get; private set; }
		public bool IsStop { get; private set; }
		
		public IObjectPool origin { get; set; }
		
		public virtual void PrepareToUse() { }
		
		public void ReturnToPool()
		{
			origin.ReturnToPool(this);
		}

		public virtual void Play()
		{
			IsPlay = true;
			IsPause = false;
			IsStop = false;
		}

		public virtual void Pause()
		{
			IsPlay = false;
			IsPause = true;
			IsStop = false;
		}

		public virtual void Stop()
		{
			IsPlay = false;
			IsPause = false;
			IsStop = true;
		}

		protected static bool IsComponentNull(Component component)
		{
			return component is null || component == null;
		}
	}
}