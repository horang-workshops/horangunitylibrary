﻿using UnityEngine;

namespace HorangLibrary
{
	public class ParticleEntity : VisualEffectEntity
	{
		public bool useAutoStopper;
		public float autoStopperTime;
		
		private new ParticleSystem particleSystem;
		private float timer;

		public override void PrepareToUse()
		{
			base.PrepareToUse();
			
			gameObject.SetActive(false);
		}

		public override void Play()
		{
			base.Play();
			
			particleSystem.Play();
		}

		public override void Pause()
		{
			base.Pause();
			
			particleSystem.Pause();
		}

		public override void Stop()
		{
			base.Stop();
			
			particleSystem.Stop();
			
			ReturnToPool();
		}

		protected override void Initialize()
		{
			particleSystem = GetComponent(typeof(ParticleSystem)) as ParticleSystem;

			if (IsComponentNull(particleSystem))
			{
				Logger.Error("파티클 시스템을 찾을 수 없습니다.");
				
				return;
			}

			if (autoStopperTime.Equals(0))
			{
				autoStopperTime = particleSystem.main.duration;
			}

			particleSystem.hideFlags = HideFlags.HideInInspector;
		}

		protected override void ObjectDestroy()
		{
			Destroy(particleSystem);
		}

		protected override void ObjectUpdate()
		{
			if (!useAutoStopper)
			{
				return;
			}
			
			timer += Time.deltaTime;

			if (timer >= autoStopperTime)
			{
				Stop();
			}
		}
	}
}